const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId => {

		return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
			if(err){
				console.log(err);
				return false;
			} else {
				return removeTask;
			}
		})
})


module.exports.updateTask = (taskId, newContent) => {


	// The findById, is a mongoose method that will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		//Result of the "findById" method will be stored in the result parameter
		result.name = newContent.name;

		// Saves the updated object in the MongoDB database
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updateTask;
			}
		})
	})
}

//Activity


module.exports.findTask = (taskId => {

		return Task.findById(taskId).then((findTask, err) => {
			if(err){
				console.log(err);
				return false;
			} else {
				return findTask;
			}
		})
})

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.status = newContent.status;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updateTask;
			}
		})
	})
}
