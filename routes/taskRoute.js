const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

//[ SECTION ] Get all tasks

router.get("/", (req, res) => {

	// It invokes the getAllTasks function from the taskController.js
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});

// [ SECTION ] Create a new task
router.post("/", (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// [SECTION] Delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// [ SECTION ] Update a task
router.put("/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// [ ACTIVITY ]
router.get("/:id", (req, res) => {

	taskController.findTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

/*router.put("/:id", (req, res) => {

	taskController.changeTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})*/





module.exports = router;